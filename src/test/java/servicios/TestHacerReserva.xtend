package servicios

import org.junit.Test
import ar.edu.unq.epers.model.ReservaException

class TestHacerReserva extends AbstractHibernateTest {

	@Test
	def void testGuardarReserva() {
		reservaManager.guardarReserva(reserva)
	}
	

	@Test(expected=ReservaException)
	def void testReservarAutoDeberiaDevolverUnaExcepcionElAutoNoTieneLaUbicaciónDeOrigenBuscada() {
		auto1.ubicacionInicial = retiro
		reserva.origen = aeroparque
		reserva.auto = auto1
		reservaManager.guardarReserva(reserva)

	}

	@Test(expected=ReservaException)
	def void testReservarAutoDeberiaDevolverUnaExcepcionElAutoNoEstaLibreEnElPeriodoPedido()
	{
		reserva.inicio = dateInicio
		reserva.fin= dateLibre
		reserva2.inicio = dateFin
		reserva.auto= auto1
		reserva2.auto=auto1
		reservaManager.guardarReserva(reserva)
		reservaManager.guardarReserva(reserva2)
		
	}
}
