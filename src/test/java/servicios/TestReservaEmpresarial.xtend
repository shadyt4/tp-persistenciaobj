package servicios

import org.junit.Test

import ar.edu.unq.epers.model.ReservaException

class TestReservaEmpresarial extends AbstractHibernateTest{
	
	@Test
	def void testHacerUnaReservaEmpresarial()
	{
		reservaManager.guardarReserva(reservaEmpresarial)	
	}
	
	
	@Test(expected=ReservaException)
	def void testCantidadMaximaDeReservasActivas()
	{
		reservaManager.guardarReserva(reservaEmpresarial)
		reservaManager.guardarReserva(reservaEmpresarial2)
		reservaManager.guardarReserva(reservaEmpresarial3)
				
		
	} 

	@Test(expected=ReservaException)
	def void testValorMaximoPorDia()
	{
		auto1.costoBase = 30000D
		reservaEmpresarial.auto=auto1
		reservaManager.guardarReserva(reservaEmpresarial)
		
			
	}
	
	 @Test(expected=ReservaException)
	def void testElUsuarioNoPerteneceALaEmpresa()
	{
		reservaEmpresarial.usuario = usuarioPrueba
		reservaManager.guardarReserva(reservaEmpresarial)
	}
	
	@Test(expected=ReservaException)
	def void testLaCategoriaNoEstaAdmitidaPorLaEmpresa()
	{
		auto1.categoria = categoriaTurismo
		reservaManager.guardarReserva(reservaEmpresarial)
	}

}
