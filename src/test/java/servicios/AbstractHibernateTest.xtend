package servicios

import ar.edu.unq.epers.model.Auto
import ar.edu.unq.epers.model.Categoria
import ar.edu.unq.epers.model.Empresa
import ar.edu.unq.epers.model.Familiar
import ar.edu.unq.epers.model.IUsuario
import ar.edu.unq.epers.model.Reserva
import ar.edu.unq.epers.model.ReservaEmpresarial
import ar.edu.unq.epers.model.Turismo
import ar.edu.unq.epers.model.Ubicacion
import ar.edu.unq.epers.model.UsuarioPrueba
import java.util.Date
import java.util.List
import org.hibernate.SessionFactory
import org.junit.Before

class AbstractHibernateTest {
	private SessionFactory sessionFactory;

	
	
	new() {
		super()
	}

	protected Auto auto1
	protected Auto auto2
	protected Auto auto3
	protected Auto auto4
	protected List<Auto> autos
	
	protected Categoria categoriaFamiliar
	protected Categoria categoriaTurismo
	
	protected Ubicacion retiro
	protected Ubicacion aeroparque
	protected Ubicacion la_plata
	protected Ubicacion ubicacionOrigen
	
	protected IUsuario usuarioPrueba
	protected IUsuario usuarioEmpresa
	protected Empresa empresa
		
	protected Date dia
	protected Date dateInicio
	protected Date dateFin
	
	protected Reserva reserva
	protected Reserva reserva2
	protected Reserva reserva3
	protected ReservaEmpresarial reservaEmpresarial
	protected ReservaEmpresarial reservaEmpresarial2
	protected ReservaEmpresarial reservaEmpresarial3
	
	protected AutoManager autoManager
	protected ReservaManager reservaManager
	
	protected Date dateLibre
	protected Date dateInicio2
	protected Date dateFin2
	protected Date dateFin3
	
	
	@Before
	def void setUp() {
		categoriaFamiliar = new Familiar =>[nombre="familiar"]
		categoriaTurismo = new Turismo =>[nombre="turismo"]
		
		retiro = new Ubicacion("Retiro")
		aeroparque = new Ubicacion("Aeroparque")
		la_plata= new Ubicacion ("La plata")
		
		auto1 = new Auto("Peugeot", "505", 1990, "XXX123", categoriaFamiliar, 100D, retiro)
		auto2 = new Auto("Ford", "ka", 2000, "XXX124", categoriaFamiliar, 100D, aeroparque)
		auto3 = new Auto("Fiat", "147", 1980, "ccc123", categoriaFamiliar, 10D, retiro)
		
		dateInicio=new Date(2015,6,11)
		dateFin= new Date (2015,6,12)
		dateLibre=new Date (2015,6,13)
		dateInicio2=new Date (2015,6,14)
		dateFin2= new Date (2015,6,15)
		dateFin3=new Date (2015,6,17)
		
		usuarioPrueba= new UsuarioPrueba("testdummy")
		usuarioEmpresa = new UsuarioPrueba("testdummy")
		
		empresa = new Empresa => [
			cuit = "30-11223344-90"
			nombreEmpresa = "Empresa Fantasmita"
			cantidadMaximaDeReservasActivas = 2
			valorMaximoPorDia = 1000D
		]
		
		empresa.usuarios.add(usuarioEmpresa)
		empresa.categoriasAdmitidas.add(categoriaFamiliar)
		reserva= new Reserva (1, retiro,aeroparque,dateInicio, dateFin,auto1,usuarioEmpresa)
		reserva2=new Reserva (2,aeroparque, retiro,dateFin, dateInicio2,auto2,usuarioEmpresa)
		reserva3= new Reserva (3,retiro, la_plata,dateInicio2,dateFin2,auto3,usuarioEmpresa )
		
		reservaEmpresarial= new ReservaEmpresarial()
			reservaEmpresarial.empresa=empresa
			reservaEmpresarial.nombreContacto="nombreContacto"
			reservaEmpresarial.cargoContacto="cargoContacto"
			reservaEmpresarial.numeroSolicitud=4
			reservaEmpresarial.origen= retiro
			reservaEmpresarial.destino =la_plata
			reservaEmpresarial.inicio=dateFin2
			reservaEmpresarial.fin=dateFin3
			reservaEmpresarial.auto=auto1
			reservaEmpresarial.usuario=usuarioEmpresa
		
		reservaEmpresarial2= new ReservaEmpresarial()
			reservaEmpresarial2.empresa=empresa
			reservaEmpresarial2.nombreContacto="nombreContacto"
			reservaEmpresarial2.cargoContacto="cargoContacto"
			reservaEmpresarial2.numeroSolicitud=5
			reservaEmpresarial2.origen= retiro
			reservaEmpresarial2.destino =la_plata
			reservaEmpresarial2.inicio=dateFin2
			reservaEmpresarial2.fin=dateFin3
			reservaEmpresarial2.auto=auto2
			reservaEmpresarial2.usuario=usuarioEmpresa
	
		reservaEmpresarial3= new ReservaEmpresarial()
			reservaEmpresarial3.empresa=empresa
			reservaEmpresarial3.nombreContacto="nombreContacto"
			reservaEmpresarial3.cargoContacto="cargoContacto"
			reservaEmpresarial3.numeroSolicitud=6
			reservaEmpresarial3.origen= retiro
			reservaEmpresarial3.destino =la_plata
			reservaEmpresarial3.inicio=dateFin2
			reservaEmpresarial3.fin=dateFin3
			reservaEmpresarial3.auto=auto3
			reservaEmpresarial3.usuario=usuarioEmpresa
		
		reservaManager=new ReservaManager
		autoManager = new AutoManager
		
	}
	
	
	
	def crearDia(Integer anio, Integer mes, Integer dia)
	{
		new Date(anio, mes,dia)
	}

}
	
