package servicios

import org.junit.Test
import org.junit.Assert
import static org.junit.Assert.*

class TestObtenerAutosDisponibles extends AbstractHibernateTest{
	
	
	@Test	
	def void obtenerAutosDisponiblesEnLaplataEl13DeJunio()  
	{
		reserva=reservaManager.guardarReserva(reserva)
		reserva=reservaManager.guardarReserva(reserva2)
		reserva=reservaManager.guardarReserva(reserva3)
		
		autos=autoManager.obtenerAutos([String a | autoManager.obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado.apply(dateLibre,la_plata)])
		assertEquals(autos.size,1)
		
	}
	
	@Test
	def void obtenerAutosDisponibles()
	{
//		dateInicio=new Date(2015,6,11)
//		dateFin= new Date (2015,6,12)
//		dateLibre=new Date (2015,6,13)
//		dateInicio2=new Date (2015,6,14)
//		dateFin2= new Date (2015,6,15)
//		dateFin3=new Date (2015,6,17)
		autoManager.guardarAuto(auto1)
		
		reserva2.inicio = dateInicio
		reserva2.fin = dateFin2
//		
		reserva3.inicio = dateInicio2
		reserva3.fin = dateFin2
//		
		reserva=reservaManager.guardarReserva(reserva2)
		reserva=reservaManager.guardarReserva(reserva3)
		
		assertEquals(autoManager.obtenerAutos([String s|autoManager.autosQueNoTienenReservas.apply(dateLibre)]).size,2)
		
	}
	
	
	
}