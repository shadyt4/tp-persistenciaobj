package servicios
import static org.junit.Assert.*

import org.junit.Test

class TestObtenerAutosPorCategoria extends AbstractHibernateTest{
	
	@Test
	def void testObtenerAutoPorCategoriaFamiliarDeberiaDevolverTresAutos()
	{
			reservaManager.guardarReserva(reserva)
			reservaManager.guardarReserva(reserva2)
			reservaManager.guardarReserva(reserva3)
			
			autos=reservaManager.obtenerAutos[reservaManager.obtenerAutosPorCategoria.apply(categoriaFamiliar)]
			
			assertEquals(autos.size,3)
			
		
	}
	
}