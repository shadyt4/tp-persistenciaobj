package servicios

import servicios.AbstractHibernateTest

import org.junit.Test
import static org.junit.Assert.*

class TestObtenerAutosPorFecha extends AbstractHibernateTest {
	
	
	@Test
	def void testObtenerAutosQueTerminaSuReservaElDia()
	{
		reserva=reservaManager.guardarReserva(reserva)
		reservaManager.obtenerAutos([reservaManager.obtenerAutosPorFechaFinDeReserva.apply(dateFin)])
	}
	
	@Test
	def void testObtenerAutosQueComienzanSuReservaElDia()
	{
		reserva=reservaManager.guardarReserva(reserva)
		reservaManager.obtenerAutos([String a | reservaManager.obtenerAutosPorFechaCominezoDeReserva.apply(dateInicio)])
	}
	
	@Test
	def void testAlBuscarAutosQueEstanaLibresParaEl11DeJunioDeberiaObtenerUnAuto()
	{
		reserva=reservaManager.guardarReserva(reserva)
		reserva=reservaManager.guardarReserva(reserva2)
		reserva=reservaManager.guardarReserva(reserva3)
		
		autos=reservaManager.obtenerAutos([String a | reservaManager.obtenerAutosQueEstanLibresLaFechaIndicada.apply(dateLibre)])
		assertEquals(autos.size,1)
	}
	
}