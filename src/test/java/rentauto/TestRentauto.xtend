package rentauto

import org.junit.Test
import org.joda.time.DateTime
import org.junit.Assert
import static org.mockito.Mockito.*
import org.junit.After

class TestRentauto {
	 
@Test def testIngresarUsuario()
	{
		var ISistema sistema= new Sistema()
		var DateTime fechaDeNee = new DateTime(84,6,11,0,0)
		var Usuario usuario1 = new Usuario ("Gabriel","Sor","uno","uno.com","pass",fechaDeNee)
		sistema.registrarUsuario(usuario1)
		var us =sistema.ingresarUsuario("uno","pass")
		Assert.assertEquals("uno",us.nombreDeUsuario) 
		
		
	}
	
@Test (expected= ValidacionException )def testValidarCuentaErroneo ()
{
		var ISistema sistema= new Sistema()
		var DateTime fechaDeNee = new DateTime(84,6,11,0,0)
		var Usuario usuario1 = new Usuario ("Gabriel","Sor","dos","dos.com","pass",fechaDeNee)
		sistema.registrarUsuario(usuario1)
		sistema.validarCuenta("codigoInvalido")
	
}

@Test def testValidarCuenta ()
{
		var ISistema sistema= new Sistema()
		var DateTime fechaDeNee = new DateTime(84,6,11,0,0)
		var Usuario usuario1 = new Usuario ("Gabriel","Sor","tres","tres.com","pass",fechaDeNee)
		sistema.registrarUsuario(usuario1)
		sistema.validarCuenta("tres"+fechaDeNee.toString)
		
	
}
	
	
	
	@Test def testcambiarPassword()
	{
		var ISistema sistema= new Sistema()
		var DateTime fechaDeNee = new DateTime(84,6,11,0,0)
		var Usuario usuario1 = new Usuario ("Gabriel","Sor","cuatro","cuatro.com","pass",fechaDeNee)
		sistema.registrarUsuario(usuario1)
		var us =sistema.ingresarUsuario("cuatro","pass")
		sistema.cambiarPassword("cuatro","pass","nuevopass")
		us = sistema.ingresarUsuario("cuatro","nuevopass")
		Assert.assertEquals("nuevopass",us.password) 
		
	}
	
	@Test
	  (expected = UsuarioYaExisteException) def testRegistrarUsuarioDeberiaLanzarUnaExcepcion(){
		
		var ISistema sistema= new Sistema()
		var DateTime fechaDeNee = new DateTime(84,6,11,0,0)
		var Usuario usuario1 = new Usuario ("Gabriel","Sor","cinco","cinco.com","pass",fechaDeNee)
		sistema.registrarUsuario(usuario1)
		
		var us = new Usuario ("Gabriel","Sor","cinco","cinco.com","pass",fechaDeNee)
		sistema.registrarUsuario(us)
		
	
	} 
	
	@Test
	  (expected = UsuarioNoExiste) def testIngresarUsuarioDeberiaLanzarUnaExcepcion(){
		
		var ISistema sistema= new Sistema()
		var DateTime fechaDeNee = new DateTime(84,6,11,0,0)
		var Usuario usuario1 = new Usuario ("Gabriel","Sor","seis","seis.com","pass",fechaDeNee)
		sistema.registrarUsuario(usuario1)
		var us =sistema.ingresarUsuario("usuarioNoExistente","pass")
		
	}
	@Test
	(expected = EnviarMailException)def void testMailFallando()
	{
		var enviadorDeMail = new EnviadorDeMailsImp()
		var mail = new Mail("","","","")

		enviadorDeMail.enviarMail(mail)


		
		
	}
	
	 @Test def void testMailAndando()
	{
		var enviadorDeMail = new EnviadorDeMailsImp()
		var mail = new Mail("cuerpo","asunto","usuario","sistema")

		enviadorDeMail.enviarMail(mail)
	}
	
	
}