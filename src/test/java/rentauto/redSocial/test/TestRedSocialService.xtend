package rentauto.redSocial.test

import rentauto.redSocial.model.RedSocialUser
import org.junit.Before
import ar.edu.unq.epers.model.UsuarioPrueba
import org.junit.Test
import org.junit.Assert
import org.junit.After
import rentauto.redSocial.model.RedSocialMessage
import org.neo4j.kernel.impl.util.register.NeoRegister.Node
import rentauto.redSocial.persNeo4J.RedSocialService

class TestRedSocialService {
	
	RedSocialService rsService
	RedSocialUser user1 
	RedSocialUser user2
	RedSocialUser user3
	RedSocialUser user4	
	
	@Before
	def void setUp(){
		rsService = new RedSocialService()
		
		user1 = new RedSocialUser(new UsuarioPrueba("Gabriel") =>[id = 31])
		user2 = new RedSocialUser(new UsuarioPrueba("Emiliano") =>[id = 24])
		user3 = new RedSocialUser(new UsuarioPrueba("Nicolas") =>[id = 23])
		user4 = new RedSocialUser(new UsuarioPrueba("Fabrizio") =>[id = 22])
		
		rsService.crearNodo(user1)
		rsService.crearNodo(user2)
		rsService.crearNodo(user3)
		rsService.crearNodo(user4)
	}
	
	@After
	def void after(){
		rsService.eliminarNodo(user1)
		rsService.eliminarNodo(user2)
		rsService.eliminarNodo(user3)
		rsService.eliminarNodo(user4)
	}
	
	
	@Test
	def void VerificoSiDadosDosUsuariosSeEstableceLaRelacionDeAmistadEntreEllos(){
			
		rsService.generarAmistad(user1, user2)
		rsService.generarAmistad(user1, user3)
		
		Assert.assertEquals(2, rsService.getAmistades(user1).length)
	}
	
	@Test
	def void VerificoSIDadoDosUsuriosQueSeEnvianMensajesAmbosRelacionaCorrectamenteCadaUnoDeSusMensajes(){
		var msg1 = new RedSocialMessage("Hola")
		var msg2 = new RedSocialMessage("Hola!")
		var msg3 = new RedSocialMessage("Como andas?")
		var msg4 = new RedSocialMessage("Bien vos??")
		var msg5 = new RedSocialMessage("Todo bien")
		
		rsService.guardarMensaje(user1,msg1,user2)
		rsService.guardarMensaje(user2,msg2,user1)
		rsService.guardarMensaje(user1,msg3,user2)
		rsService.guardarMensaje(user2,msg4,user1)
		rsService.guardarMensaje(user1,msg5,user2)
		
		
	
		Assert.assertEquals(3, rsService.mensajesEnviadosPor(user1).length)
		Assert.assertEquals(2, rsService.mensajesEnviadosPor(user2).length)
		
		Assert.assertEquals(2, rsService.mensajesRecibidosPor(user1).length)
		Assert.assertEquals(3, rsService.mensajesRecibidosPor(user2).length)
		

	}
	
	@Test
	def void DadoUnGrupoDeAmigosConsultosPorLosMisAmigosYAmigosDeMisAmigos(){
		
		rsService.generarAmistad(user1, user2)
		rsService.generarAmistad(user2, user3)
		rsService.generarAmistad(user2, user4)
		
		Assert.assertEquals(3, rsService.amigosyamigosDeLosAmigosDe(user1).size())
	}
	
	@Test
	def void DadoUnGrupoDeAmigosConsultosPorLosMisAmigosYAmigosDeMisAmigosbis(){
		
		rsService.generarAmistad(user1, user2)
		rsService.generarAmistad(user2, user3)
		rsService.generarAmistad(user3, user4)
		
		Assert.assertEquals(3, rsService.amigosyamigosDeLosAmigosDe(user1).size())
	}	

}