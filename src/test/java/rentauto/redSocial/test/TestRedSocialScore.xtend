package rentauto.redSocial.test

import rentauto.redSocial.model.RedSocialScore
import org.junit.Before
import ar.edu.unq.epers.model.UsuarioPrueba
import rentauto.redSocial.model.RedSocialUser
import ar.edu.unq.epers.model.Auto
import ar.edu.unq.epers.model.Ubicacion
import ar.edu.unq.epers.model.Familiar
import org.junit.Test
import org.junit.After
import static extension org.junit.Assert.*
import rentauto.redSocial.model.Score
import rentauto.redSocial.model.Visibilidad
import rentauto.redSocial.model.ScoreManager
import org.mockito.Mock
import rentauto.redSocial.persNeo4J.RedSocialService
import org.mockito.Mockito
import java.util.ArrayList
import ar.edu.unq.epers.model.TodoTerreno

class TestRedSocialScore {
	
	var RedSocialUser user1
	var Auto auto
	var ScoreManager manager
		
	@Before
	def void setUp(){
		user1 = new RedSocialUser(new UsuarioPrueba("Gabriel") =>[id = 31])
		auto = new Auto ("Ford", "Focus", 2009, "AAA123", new Familiar(), 2.30, new Ubicacion("Wilde"))
		manager = new ScoreManager()
	}
	
	@Test
	def void dadoUnUsuarioYUnAutoLoCaLificoYLoGuardoEnLaBaseDeDatos(){
		
		var rsScore = new RedSocialScore(user1, auto, Score.EXCELENTE, "muy buen auto", Visibilidad.PUBLICO)
		
		manager.saveScore(rsScore)

		var scores = manager.getScoreForUser(user1)
		var score = scores.get(0)
		
		assertEquals(score.autoId,"AAA123")
	}
	
	@Test
	def void dadoUnUsuarioConSultoPorSusCalificacionesPublicas(){
		val rsScore = new RedSocialScore(user1, auto, Score.EXCELENTE, "muy buen auto", Visibilidad.PUBLICO)
		val rsScore1 = new RedSocialScore(user1, auto, Score.EXCELENTE, "muy buen auto", Visibilidad.PRIVADO)
		
		manager.saveScore(rsScore)
		manager.saveScore(rsScore1)
		
		var scores = manager.getPublicScoreForUser(user1)
		
		assertTrue(scores.get(0).equals(rsScore))
	}
	
	@Test
	def void dadoUnUsuarioConnsultoPorSusCalificacionsPrivadas()
	{
		val rsScore = new RedSocialScore(user1, auto, Score.EXCELENTE, "muy buen auto", Visibilidad.PUBLICO)
		val rsScore1 = new RedSocialScore(user1, auto, Score.EXCELENTE, "muy buen auto", Visibilidad.PRIVADO)
		
		manager.saveScore(rsScore)
		manager.saveScore(rsScore1)
		
		var scores = manager.getPrivateScoreForUser(user1)

		assertTrue(scores.get(0).equals(rsScore1))
	}
	
	@Test
	def void dadoUnUsuarioConsultaPorLasCalificacionesDeSusAmigos(){
		
		var user2 = new RedSocialUser(new UsuarioPrueba("Fabrizio") =>[id = 24])
		var user3 = new RedSocialUser(new UsuarioPrueba("Nicolas") =>[id = 23])
		var user4 = new RedSocialUser(new UsuarioPrueba("Emiliano") =>[id = 25])
		var user5 = new RedSocialUser(new UsuarioPrueba("Ronny") =>[id = 27])
		
		var auto1 = new Auto ("Ford", "Focus", 2008, "AAA123", new Familiar(), 2.30, new Ubicacion("Wilde"))
		var auto2 = new Auto ("Ford", "Focus", 2009, "AAA122", new Familiar(), 2.30, new Ubicacion("Wilde"))
		var auto3 = new Auto ("Ford", "Mondeo", 2008, "AAA321", new Familiar(), 2.30, new Ubicacion("Wilde"))
		var auto4 = new Auto ("Ford", "Fiesta", 2008, "AAA231", new Familiar(), 2.30, new Ubicacion("Wilde"))
		var auto5 = new Auto ("Ford", "EcoSports", 2008, "AAA111", new TodoTerreno(), 2.30, new Ubicacion("Wilde"))
		
		var amigosDeUser1 = new ArrayList<RedSocialUser>()
		amigosDeUser1.add(user2)
		amigosDeUser1.add(user3)
		
		var amigosDeUser2 = new ArrayList<RedSocialUser>()
		amigosDeUser2.add(user4)
		amigosDeUser2.add(user1)
		
		var amigoDeUser4 = new ArrayList<RedSocialUser>()
		amigoDeUser4.add(user5)
		
		manager.saveScore(new RedSocialScore(user4, auto2, Score.EXCELENTE, "muy buen auto", Visibilidad.PUBLICO))
		manager.saveScore(new RedSocialScore(user2, auto1, Score.BUENO, "CORRECTO", Visibilidad.PUBLICO))
		manager.saveScore(new RedSocialScore(user2, auto3, Score.EXCELENTE, "muy buen auto", Visibilidad.SOLO_AMIGOS))
		manager.saveScore(new RedSocialScore(user2, auto4, Score.REGULAR, "muy caro", Visibilidad.PRIVADO))
		manager.saveScore(new RedSocialScore(user3, auto5, Score.EXCELENTE, "muy buen auto", Visibilidad.PUBLICO))
		manager.saveScore(new RedSocialScore(user5, auto5, Score.BUENO, "comodo", Visibilidad.PRIVADO))
		

	/*
	 * aqui habria que mockear el service del Neo4j para pedir los amigos, pero como
	 *  el mock se le setearia par que devuelve al lista y asi pasarsela como parametro
	 * 	al mensaje del ScoreManager, directamente le paso la lista de usuario
	 */
	 		
		assertEquals(manager.getScoresOfFriends(amigosDeUser1).size(), 3)
		assertEquals(manager.getScoresOfFriends(amigosDeUser2).size(), 1)
		assertEquals(manager.getScoresOfFriends(amigoDeUser4).size(), 0)
		
	}
	
	@After
	def void cleanDB(){
		manager.cleanDB()
	}
}