package cache

import ar.edu.unq.epers.model.Auto
import java.util.List
import ar.edu.unq.epers.model.Categoria
import ar.edu.unq.epers.model.Ubicacion
import ar.edu.unq.epers.model.IUsuario
import ar.edu.unq.epers.model.Empresa
import java.util.Date
import ar.edu.unq.epers.model.Reserva
import ar.edu.unq.epers.model.ReservaEmpresarial
import servicios.AutoManager
import servicios.ReservaManager
import ar.edu.unq.epers.model.Familiar
import ar.edu.unq.epers.model.Turismo
import ar.edu.unq.epers.model.UsuarioPrueba
import org.junit.Before

class AbstractTestCache {
	
	/**Esta clase inicializa los autos y las reservas 
	 * las carga en la tabla
	 * para poder usarlos en el test de hbase
	 */ 
	
	
	protected Auto auto1
	protected Auto auto2
	protected Auto auto3
	protected Auto auto4
	protected List<Auto> autos

	protected Categoria categoriaFamiliar
	protected Categoria categoriaTurismo

	protected Ubicacion retiro
	protected Ubicacion aeroparque
	protected Ubicacion la_plata
	protected Ubicacion ubicacionOrigen

	protected IUsuario usuarioPrueba
	protected IUsuario usuarioEmpresa
	protected Empresa empresa

	protected Date dia
	protected Date dateInicio
	protected Date dateFin
	protected Date dateInicio5
	protected Date dateFin5
	protected Reserva reserva
	protected Reserva reserva2
	protected Reserva reserva3
	Reserva reserva4

	Reserva reserva5

	Reserva reserva6

	protected Reserva reserva7

	Reserva reserva8

	Reserva reserva9

	Reserva reserva10

	Reserva reserva11
	protected Reserva reserva12

	protected ReservaEmpresarial reservaEmpresarial
	protected ReservaEmpresarial reservaEmpresarial2
	protected ReservaEmpresarial reservaEmpresarial3

	protected AutoManager autoManager
	protected ReservaManager reservaManager

	protected Date dateLibre
	protected Date dateInicio2
	protected Date dateFin2
	protected Date dateFin3

	Auto auto5

	Auto auto6

	Auto auto7

	Auto auto12

	Auto auto11

	Auto auto9

	Auto auto10

	protected Auto auto8

	protected Date dateInicio3

	protected Date dateInicio4

	protected Date dateQueNoAnda

	@Before
	def void setup() {
		categoriaFamiliar = new Familiar => [nombre = "familiar"]
		categoriaTurismo = new Turismo => [nombre = "turismo"]

		retiro = new Ubicacion("Retiro")
		aeroparque = new Ubicacion("Aeroparque")
		la_plata = new Ubicacion("La plata")

		auto1 = new Auto("Peugeot", "505", 1990, "XXX123", categoriaFamiliar, 100D, retiro)
		auto2 = new Auto("Ford", "ka", 2000, "XXX124", categoriaFamiliar, 100D, aeroparque)
		auto3 = new Auto("Fiat", "duna", 1980, "ccc123", categoriaFamiliar, 10D, retiro)
		auto4 = new Auto("Peugeot", "505", 1990, "XXX123", categoriaFamiliar, 100D, la_plata)
		auto5 = new Auto("Ford", "fiesta", 2000, "XXX124", categoriaFamiliar, 100D, aeroparque)
		auto6 = new Auto("Fiat", "147", 1980, "ccc123", categoriaFamiliar, 10D, retiro)
		auto7 = new Auto("Peugeot", "505", 1990, "XXX123", categoriaFamiliar, 100D, retiro)
		auto8 = new Auto("Ford", "focus", 2000, "XXX124", categoriaFamiliar, 100D, aeroparque)
		auto9 = new Auto("Fiat", "DeathStar", 1980, "ccc123", categoriaFamiliar, 10D, retiro)
		auto10 = new Auto("Peugeot", "504", 1990, "XXX123", categoriaFamiliar, 100D, retiro)
		auto11 = new Auto("Delorian", "BackToTheF", 2000, "XXX124", categoriaFamiliar, 100D, aeroparque)
		auto12 = new Auto("Chevrolet", "Cherokee", 1980, "ccc123", categoriaFamiliar, 10D, retiro)
		
		dateInicio = new Date(2015, 6, 1)
		dateFin = new Date(2015, 6, 2)
		dateInicio2 = new Date(2015, 6, 4)
		dateInicio3 = new Date(2015, 6, 5)
		dateFin2 = new Date(2015, 6, 6)
		dateFin3 = new Date(2015, 6, 7)
		dateInicio4 = new Date(2015, 6, 8)
		dateQueNoAnda = new Date(2015, 6, 9)
		dateLibre = new Date(2015,6,10)
		dateInicio5= new Date(2015,7,1)
		dateFin5=new Date(2015,7,2)
		usuarioPrueba = new UsuarioPrueba("testdummy")
		usuarioEmpresa = new UsuarioPrueba("testdummy")

		empresa = new Empresa => [
			cuit = "30-11223344-90"
			nombreEmpresa = "Empresa Fantasmita"
			cantidadMaximaDeReservasActivas = 2
			valorMaximoPorDia = 1000D
		]

		empresa.usuarios.add(usuarioEmpresa)
		empresa.categoriasAdmitidas.add(categoriaFamiliar)

		reserva = new Reserva(1, retiro, la_plata, dateInicio, dateFin, auto1, usuarioEmpresa)
		reserva2 = new Reserva(2, aeroparque, retiro, dateFin, dateInicio2, auto2, usuarioEmpresa)
		reserva3 = new Reserva(3, retiro, la_plata, dateInicio2, dateFin2, auto3, usuarioEmpresa)
		reserva4 = new Reserva(4, retiro, aeroparque, dateInicio, dateFin, auto4, usuarioEmpresa)
		reserva5 = new Reserva(5, aeroparque, retiro, dateFin, dateInicio2, auto5, usuarioEmpresa)
		reserva6 = new Reserva(6, retiro, la_plata, dateInicio2, dateFin2, auto6, usuarioEmpresa)
		reserva7 = new Reserva(7, retiro, la_plata, dateLibre, dateFin5, auto7, usuarioEmpresa)
		reserva8 = new Reserva(8, aeroparque, retiro, dateFin, dateInicio2, auto8, usuarioEmpresa)
		reserva9 = new Reserva(9, retiro, la_plata, dateInicio2, dateFin2, auto9, usuarioEmpresa)
		reserva10 = new Reserva(10, retiro, aeroparque, dateInicio, dateFin, auto10, usuarioEmpresa)
		reserva11 = new Reserva(11, aeroparque, retiro, dateFin, dateInicio2, auto11, usuarioEmpresa)
		reserva12 = new Reserva(12, retiro, la_plata, dateInicio2, dateQueNoAnda, auto12, usuarioEmpresa)

		reservaEmpresarial = new ReservaEmpresarial()
		reservaEmpresarial.empresa = empresa
		reservaEmpresarial.nombreContacto = "nombreContacto"
		reservaEmpresarial.cargoContacto = "cargoContacto"
		reservaEmpresarial.numeroSolicitud = 4
		reservaEmpresarial.origen = retiro
		reservaEmpresarial.destino = la_plata
		reservaEmpresarial.inicio = dateFin2
		reservaEmpresarial.fin = dateFin3
		reservaEmpresarial.auto = auto1
		reservaEmpresarial.usuario = usuarioEmpresa

		reservaEmpresarial2 = new ReservaEmpresarial()
		reservaEmpresarial2.empresa = empresa
		reservaEmpresarial2.nombreContacto = "nombreContacto"
		reservaEmpresarial2.cargoContacto = "cargoContacto"
		reservaEmpresarial2.numeroSolicitud = 5
		reservaEmpresarial2.origen = retiro
		reservaEmpresarial2.destino = la_plata
		reservaEmpresarial2.inicio = dateFin2
		reservaEmpresarial2.fin = dateFin3
		reservaEmpresarial2.auto = auto2
		reservaEmpresarial2.usuario = usuarioEmpresa

		reservaEmpresarial3 = new ReservaEmpresarial()
		reservaEmpresarial3.empresa = empresa
		reservaEmpresarial3.nombreContacto = "nombreContacto"
		reservaEmpresarial3.cargoContacto = "cargoContacto"
		reservaEmpresarial3.numeroSolicitud = 6
		reservaEmpresarial3.origen = retiro
		reservaEmpresarial3.destino = la_plata
		reservaEmpresarial3.inicio = dateFin2
		reservaEmpresarial3.fin = dateFin3
		reservaEmpresarial3.auto = auto3
		reservaEmpresarial3.usuario = usuarioEmpresa

		reservaManager = new ReservaManager
		autoManager = new AutoManager

		reserva = reservaManager.guardarReserva(reserva)

		reserva = reservaManager.guardarReserva(reserva2)
		reserva = reservaManager.guardarReserva(reserva3)
		

	}
}
