package cache

import org.junit.Test
import static org.junit.Assert.*
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client.HBaseAdmin
import org.apache.hadoop.hbase.HTableDescriptor
import org.apache.hadoop.hbase.HColumnDescriptor
import org.apache.hadoop.hbase.client.HTable
import org.junit.After
import org.junit.Before
import cacheHome.BorrarTabla
import cacheHome.CacheRentautoDAO
import java.util.List
import ar.edu.unq.epers.model.Auto
import java.util.ArrayList
import cacheHome.InsertarAutos
import cacheHome.HBaseSessionManager
import cacheServices.Servicios

class TestBuscarReservas extends AbstractTestCache {

	@Test
	def void test() {


		var List<Auto> autosDisponibles = new ArrayList<Auto>()

		var session = HBaseSessionManager.iniciar_sesion

		var HTableDescriptor rentautoDesc = new HTableDescriptor("rentauto");
		var HColumnDescriptor autosHB = new HColumnDescriptor("autos");

		rentautoDesc.addFamily(autosHB);

		session.admin.createTable(new HTableDescriptor(rentautoDesc));

		autos = autoManager.obtenerAutos(
			[String a|autoManager.obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado.apply(dateQueNoAnda, retiro)])
		assertEquals(autos.size, 1)
		System.out.println(autos.get(0).marca) //deberia se ford
		assertEquals(autos.get(0).marca, "Ford")
		
		Servicios.insertar_autos(autos,dateQueNoAnda,retiro,session.conf,"rentauto","autos","fecha","ubicacion","auto")
		
		autosDisponibles = Servicios.obtener_autos(dateQueNoAnda, retiro, "rentauto", session.conf)
		assertEquals(autosDisponibles.size, 1)
		for (auto : autosDisponibles) {

			System.out.println(auto.marca)

		}

	}

	@After
	def void tearDown() {

		var Configuration conf = HBaseConfiguration.create();
		var BorrarTabla br = new BorrarTabla()
		br.borrar(conf, "rentauto")

	//
	}
}
