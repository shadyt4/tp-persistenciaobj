package cache

import org.junit.Test
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.client.ResultScanner
import org.apache.hadoop.hbase.client.Scan
import org.apache.hadoop.hbase.client.Result
import cacheHome.HBaseSessionManager
import org.apache.hadoop.hbase.HTableDescriptor
import org.apache.hadoop.hbase.HColumnDescriptor
import org.junit.After
import org.apache.hadoop.hbase.client.HBaseAdmin
import cacheHome.BorrarTabla

class TestTamanhoDeLaTabla {
	
	
	
	@Test
	def void testTamanhoDeLaTabla(){
		var index=0
			var session = HBaseSessionManager.iniciar_sesion
//		 new HBaseAdmin(conf);
		var Configuration conf = HBaseConfiguration.create();
		conf.addResource("conf/hbase-site.xml");
		conf.set("hbase.master", "localhost:60000");
		var admin = new HBaseAdmin(conf);
		var HTableDescriptor rentautoDesc = new HTableDescriptor("rentauto");
		var HColumnDescriptor autosHB = new HColumnDescriptor("autos");

		rentautoDesc.addFamily(autosHB);

		session.admin.createTable(new HTableDescriptor(rentautoDesc));

		var HTable table = new HTable(conf, "rentauto") // seria un table.open()
		
		System.out.println("scanning full table:");
		var ResultScanner scanner = table.getScanner(new Scan());
		var Result rr = scanner.next()
		while ( rr != null) {
  		System.out.println(index)
			index=index+1
			rr = scanner.next();
		}
		
		
		
	}
	
	@After
	def void tearDown() {
		
		var Configuration conf = HBaseConfiguration.create();
		var BorrarTabla br = new BorrarTabla()
		br.borrar(conf,"rentauto")

	//
	}
}