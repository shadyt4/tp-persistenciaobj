package cache

import ar.edu.unq.epers.model.Auto
import cacheHome.BorrarTabla
import cacheHome.CacheRentautoDAO
import cacheServices.Servicios
import java.util.ArrayList
import java.util.List
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.HColumnDescriptor
import org.apache.hadoop.hbase.HTableDescriptor
import org.apache.hadoop.hbase.client.HBaseAdmin
import org.apache.hadoop.hbase.client.HTable
import org.junit.After
import org.junit.Test
import servicios.AbstractHibernateTest
import servicios.RentautoServices

class TestCache  extends AbstractHibernateTest{

	//i want to tell you a story 
	//about a little man
	//if i can
	@Test
	def void test() {
		
		
		var RentautoServices r_services = new RentautoServices()
		

		var CacheRentautoDAO crdao= new CacheRentautoDAO()
		var List<Auto>autosDisponibles = new ArrayList<Auto>()
		//esto sería como el session factory del hbase
		var Configuration conf = HBaseConfiguration.create();
		conf.addResource("conf/hbase-site.xml");
		conf.set("hbase.master", "localhost:60000");
		var admin = new HBaseAdmin(conf);

		var HTableDescriptor rentautoDesc = new HTableDescriptor("rentauto");
		var HColumnDescriptor autosHB = new HColumnDescriptor("autos");

		rentautoDesc.addFamily(autosHB);

		admin.createTable(new HTableDescriptor(rentautoDesc));

		var HTable table
				//este es el auto disponible que hay que guardar
				
				reserva = reservaManager.guardarReserva(reserva)
		
				reserva = reservaManager.guardarReserva(reserva2)
				reserva = reservaManager.guardarReserva(reserva3)
		
				//aca obtengo los autos que estan disponibles segun la consulta por hibernate
				autos = autoManager.obtenerAutos(
					[String a|autoManager.obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado.apply(dateLibre, la_plata)])
		
				
		Servicios.insertar_autos(autos,dateLibre,la_plata,conf,"rentauto","autos","fecha","ubicacion","auto")
		
		autosDisponibles=Servicios.obtener_autos(dateLibre,la_plata, "rentauto",conf)
		
		Servicios.actualizar_autos(dateLibre,la_plata,"rentauto",conf)
		for (auto : autosDisponibles) {
			
			System.out.println(auto.marca)
			
		}
	}

	@After
	def void tearDown() {
		
		var Configuration conf = HBaseConfiguration.create();
		var BorrarTabla br = new BorrarTabla()
		br.borrar(conf,"rentauto")

	//
	}
}
