package cache

import servicios.AbstractHibernateTest
import java.util.Collection
import ar.edu.unq.epers.model.Auto
import com.google.gson.Gson
import org.junit.Before
import org.junit.Test
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import org.apache.hadoop.hbase.util.Bytes
import java.util.List

class TestJson extends AbstractHibernateTest {

	var Collection<Auto> autos
	var Gson guason

	@Test
	def void testSerializar() {

		//serializar
		//falsearAutos
		//		 String marca
		//	 String modelo
		//	Integer anio
		//	String patente
		//	Double costoBase
		//	Categoria categoria
		var ac1 = new Auto => [
			marca = auto1.marca
			modelo = auto1.modelo
			anio = auto1.anio
			patente = auto1.patente
			costoBase = auto1.costoBase
		]
		var ac2 = new Auto => [
			marca = auto2.marca
			modelo = auto2.modelo
			anio = auto2.anio
			patente = auto2.patente
			costoBase = auto2.costoBase
		]
		var ac3 = new Auto => [
			marca = auto2.marca
			modelo = auto2.modelo
			anio = auto2.anio
			patente = auto2.patente
			costoBase = auto2.costoBase
		]

		autos = #[ac1, ac2, ac3]
		guason = new Gson()
		var String json = guason.toJson(autos)
		
		var Type collectionType = new TypeToken<Collection<Auto>>() {
		}.getType()
		var Collection<Auto> autos2 = guason.fromJson(json, collectionType)

	}

	@Test
	def void testDeserializar() {
		var ac1 = new Auto => [
			marca = auto1.marca
			modelo = auto1.modelo
			anio = auto1.anio
			patente = auto1.patente
			costoBase = auto1.costoBase
		]
		guason = new Gson()
		var String json = guason.toJson(ac1)
		var byte[] b= Bytes.toBytes(json)
		var String str = Bytes.toString(b)
		var Auto auto22 = guason.fromJson(str,Auto) 
		System.out.println(str)
		System.out.println(auto22.marca)
	}
	
	@Test
	def void testDeserializarCollection() {
		var ac1 = new Auto => [
			marca = auto1.marca
			modelo = auto1.modelo
			anio = auto1.anio
			patente = auto1.patente
			costoBase = auto1.costoBase
		]
		guason = new Gson()
		var Collection<Auto> ats= #[ac1,ac1,ac1]
		var String json = guason.toJson(ats)
		var byte[] b= Bytes.toBytes(json)
		var String str = Bytes.toString(b)
		var Type collectionType = new TypeToken<Collection<Auto>>(){}.getType()
		var Collection<Auto> ats2 = guason.fromJson(str,collectionType)
		 
		System.out.println(str)
		System.out.println(ats2.get(0).marca)
		
	}

}
