package cache

import org.junit.Test
import static org.junit.Assert.*
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client.HBaseAdmin
import org.apache.hadoop.hbase.HTableDescriptor
import org.apache.hadoop.hbase.HColumnDescriptor
import org.apache.hadoop.hbase.client.HTable
import org.junit.After
import org.junit.Before
import cacheHome.BorrarTabla
import cacheHome.CacheRentautoDAO
import java.util.List
import ar.edu.unq.epers.model.Auto
import java.util.ArrayList
import cacheHome.InsertarAutos
import cacheServices.Servicios

class TestInsertar extends AbstractTestCache {

	@Test
	def void testInsertar() {

		var List<Auto> autosDisponibles = new ArrayList<Auto>()

		//esto sería como el session factory del hbase
		var Configuration conf = HBaseConfiguration.create();
		conf.addResource("conf/hbase-site.xml");
		conf.set("hbase.master", "localhost:60000");
		var admin = new HBaseAdmin(conf);

		var HTableDescriptor rentautoDesc = new HTableDescriptor("rentauto");
		var HColumnDescriptor autosHB = new HColumnDescriptor("autos");

		rentautoDesc.addFamily(autosHB);

		admin.createTable(new HTableDescriptor(rentautoDesc));

		//var HTable table
		var InsertarAutos ia = new InsertarAutos()

		//aca obtengo los autos que estan disponibles segun la consulta por hibernate
		autos = autoManager.obtenerAutos(
			[String a|autoManager.obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado.apply(dateLibre, la_plata)])
		assertEquals(autos.size, 2)

		assertEquals(
			Servicios.insertar_autos(autos, dateLibre, la_plata, conf, "rentauto", "autos", "fecha", "ubicacion", "auto").
				size, 2)
		autosDisponibles = Servicios.obtener_autos(dateLibre, la_plata, "rentauto", conf)
		assertEquals(autosDisponibles.size, 2)

		//crdao.actualizarTabla(dateLibre,la_plata,"rentauto",conf)
		for (auto : autosDisponibles) {

			System.out.println(auto.marca)

		}

		// este es otro test pero cuando lo separo no anda así va todo junto
		var reserva12 = reservaManager.guardarReserva(reserva12)
		autos = autoManager.obtenerAutos(
			[String a|autoManager.obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado.apply(dateLibre, la_plata)])
		assertEquals(autos.size, 3)
		Servicios.actualizar_autos(dateLibre, la_plata, "rentauto", conf)
		autosDisponibles = Servicios.obtener_autos(dateLibre, la_plata, "rentauto", conf)
		assertEquals(autosDisponibles.size, 3)
		for (auto : autosDisponibles) {

			System.out.println(auto.marca)

		}

		// este es otro test
		autos = autoManager.obtenerAutos(
			[String a|autoManager.obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado.apply(dateQueNoAnda, retiro)])
		assertEquals(autos.size, 1)
		System.out.println(autos.get(0).marca) //deberia se ford
		InsertarAutos.insertar(autos, dateQueNoAnda, retiro, conf, "rentauto", "autos", "fecha", "ubicacion", "auto")

		autosDisponibles = Servicios.obtener_autos(dateQueNoAnda, retiro, "rentauto", conf)
		System.out.println(autosDisponibles)
		assertEquals(autosDisponibles.size, 1)
		for (auto : autosDisponibles) {

			System.out.println(auto.marca)

		}

	}

	
	@After
	def void tearDown() {

		var Configuration conf = HBaseConfiguration.create();
		var BorrarTabla br = new BorrarTabla()
		br.borrar(conf, "rentauto")

	//
	}
}
