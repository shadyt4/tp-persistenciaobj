package cache

import servicios.RentautoServices
import org.junit.Test
import cacheServices.Servicios
import org.apache.hadoop.hbase.HBaseConfiguration
import cacheHome.HBaseSessionManager
import org.junit.After
import org.apache.hadoop.conf.Configuration
import cacheHome.BorrarTabla
import org.apache.hadoop.hbase.client.HBaseAdmin
import org.apache.hadoop.hbase.HTableDescriptor
import org.apache.hadoop.hbase.HColumnDescriptor
import static extension org.junit.Assert.*
import ar.edu.unq.epers.model.Reserva
import rentautoDAOS.ReservaDAO

class TestActualizar extends AbstractTestCache {

	@Test
	def void test() {

		var reserva_test_actualizar = new Reserva(8, aeroparque, retiro, dateFin, dateInicio2, auto8, usuarioEmpresa)
		var session = HBaseSessionManager.iniciar_sesion

		var HTableDescriptor rentautoDesc = new HTableDescriptor("rentauto");
		var HColumnDescriptor autosHB = new HColumnDescriptor("autos");

		rentautoDesc.addFamily(autosHB);

		session.admin.createTable(new HTableDescriptor(rentautoDesc));
		assertEquals(Servicios.obtener_autos(dateInicio2, retiro, "rentauto", session.conf).size, 0)

		var RentautoServices r_services = new RentautoServices()
		r_services.reservar(reserva_test_actualizar)

		assertEquals(Servicios.obtener_autos(dateInicio2, retiro, "rentauto", session.conf).size, 1)
		var rdao = new ReservaDAO	

	}

	@After
	def void tearDown() {


		
		var Configuration conf = HBaseConfiguration.create();
		var BorrarTabla br = new BorrarTabla()
		br.borrar(conf, "rentauto")

	//
	}

}
