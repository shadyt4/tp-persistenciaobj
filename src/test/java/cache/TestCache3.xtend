package cache

import ar.edu.unq.epers.model.Auto
import cacheHome.BorrarTabla
import cacheHome.CacheRentautoDAO
import cacheHome.HBaseSessionManager
import cacheHome.InsertarAutos
import java.util.ArrayList
import java.util.List
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.HColumnDescriptor
import org.apache.hadoop.hbase.HTableDescriptor
import org.apache.hadoop.hbase.client.HTable
import org.junit.After
import org.junit.Test

import static org.junit.Assert.*

class TestCache3 extends AbstractTestCache {




	
	@Test
	def void test()   {

		var CacheRentautoDAO crdao= new CacheRentautoDAO()
		//var CacheRentautoDAO crdao2= new CacheRentautoDAO()
		
		var List<Auto>autosDisponibles = new ArrayList<Auto>()

		
		var session = HBaseSessionManager.iniciar_sesion
//		 new HBaseAdmin(conf);

		var HTableDescriptor rentautoDesc = new HTableDescriptor("rentauto");
		var HColumnDescriptor autosHB = new HColumnDescriptor("autos");

		rentautoDesc.addFamily(autosHB);

		session.admin.createTable(new HTableDescriptor(rentautoDesc));

		var HTable table
		var InsertarAutos ia = new InsertarAutos()
		var InsertarAutos ia2 = new InsertarAutos()
		
		var reserva12 =reservaManager.guardarReserva(reserva12)
			
		// este es otro test
		autos = autoManager.obtenerAutos(
			[String a|autoManager.obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado.apply(dateQueNoAnda, retiro)])
				assertEquals(autos.size, 1)
		System.out.println(autos.get(0).marca)//deberia se ford
		InsertarAutos.insertar(autos,dateQueNoAnda,retiro,session.conf,"rentauto","autos","fecha","ubicacion","auto")
						
		
		
		autosDisponibles=CacheRentautoDAO.obtenerAutosPorFechaYUbicacion(dateQueNoAnda,retiro, "rentauto",session.conf)
		assertEquals(autosDisponibles.size, 1)
		//crdao.actualizarTabla(dateLibre,la_plata,"rentauto",conf)
		for (auto : autosDisponibles) {
			
			System.out.println(auto.marca)
			
		}
		
		
		// este es otro test pero cuando lo separo no anda así va todo junto
		
		autos = autoManager.obtenerAutos(
			[String a|autoManager.obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado.apply(dateLibre, la_plata)])
				assertEquals(autos.size, 3)
			for (auto : autos) {
			
			System.out.println(auto.marca+auto.idAuto)
			
		}			
		
		assertEquals(		
		InsertarAutos.insertar(autos,dateLibre,la_plata,session.conf,"rentauto","autos","fecha","ubicacion","auto").size,3)
		autosDisponibles=CacheRentautoDAO.obtenerAutosPorFechaYUbicacion(dateLibre,la_plata, "rentauto",session.conf)
		assertEquals(autosDisponibles.size, 3)
		//crdao.actualizarTabla(dateLibre,la_plata,"rentauto",conf)
		for (auto : autosDisponibles) {
			
			System.out.println(auto.marca+auto.idAuto)
			
		}
		val autoAReservar = autosDisponibles.get(0)
		val autoParaReservar=autoManager.obtenerAutos([autoManager.obtenerAutoPorId.apply(autoAReservar.idAuto)])
		val autito =autoParaReservar.get(0)
		reserva7.auto=autito
		reserva=reservaManager.guardarReserva(reserva7)
		autosDisponibles=CacheRentautoDAO.obtenerAutosPorFechaYUbicacion(dateLibre,la_plata, "rentauto",session.conf)
		assertEquals(autosDisponibles.size, 2)
		//crdao.actualizarTabla(dateLibre,la_plata,"rentauto",conf)
		for (auto : autosDisponibles) {
			
			System.out.println(auto.marca+auto.idAuto)
			
		}
		
	}

	

	

	@After
	def void tearDown() {
		
		var Configuration conf = HBaseConfiguration.create();
		var BorrarTabla br = new BorrarTabla()
		br.borrar(conf,"rentauto")

	//
	}
}
	
