package cacheServices
import cacheHome.InsertarAutos
import cacheHome.CacheRentautoDAO

import java.util.List
import ar.edu.unq.epers.model.Auto
import java.util.Date
import ar.edu.unq.epers.model.Ubicacion
import org.apache.hadoop.conf.Configuration

class Servicios {
	
	def static insertar_autos(List<Auto> autos, Date dateDisponible, Ubicacion ubicacionDisponible,
		Configuration conf, String nombreDeLaTabla, String columnFamilyName,
		 String fecha, String ubicacion, String auto)
	{
		InsertarAutos.insertar(autos,dateDisponible,ubicacionDisponible,conf,nombreDeLaTabla,columnFamilyName,fecha,ubicacion,auto)
	}
	
	def static obtener_autos(Date dateDisponible,Ubicacion ubicacionDisponible,String nombreDeLaTabla,Configuration conf)
	{
		CacheRentautoDAO.obtenerAutosPorFechaYUbicacion(dateDisponible,ubicacionDisponible,nombreDeLaTabla,conf)
	}
	
	def static actualizar_autos(Date fecha, Ubicacion ubicacion, String nombreDeLaTabla, Configuration conf)
	{
		CacheRentautoDAO.actualizarTabla(fecha, ubicacion, nombreDeLaTabla, conf)
	}
}