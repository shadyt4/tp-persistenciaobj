package ar.edu.unq.epers.model

import ar.edu.unq.epers.model.IUsuario
import ar.edu.unq.epers.model.Reserva
import java.util.List
import java.util.ArrayList
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class UsuarioPrueba implements IUsuario{
	Integer id
	String nombre
	List<Reserva> reservas= newArrayList
	new ()
	{}
	new (String nombre)
	{
		this.nombre=nombre
	}
	
	override agregarReserva(Reserva unaReserva) {
		reservas.add(unaReserva)
	}
	
	override getReservas() {
		this.reservas
	}
	
	override getNombre() {
		this.nombre
	}
	
	def setNombre(String nombre)
	{
		this.nombre=nombre
	}
	
	override getIdUsuario() {
		this.id
	}
	
	override setIdUsuario(Integer id) {
		this.id=id
	}
	
}