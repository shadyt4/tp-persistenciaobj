package ar.edu.unq.epers.model

import java.util.List

interface IUsuario {
	def void agregarReserva(Reserva unaReserva)
	def List<Reserva> getReservas()
	//necesario para hacer consultas
	def String getNombre()
	def Integer getIdUsuario()
	def void setIdUsuario(Integer id)
	def void setReservas(List<Reserva> reserva)
}