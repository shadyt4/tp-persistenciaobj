package ar.edu.unq.epers.model

import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors 
class Ubicacion {
	String nombre
	Integer idUbicacion
	new ()
	{}	
	new(String nombre){
		this.nombre = nombre
	}
//	override  equals(Object ubicacion)
//	{
//		var Ubicacion ubi
//		if (ubicacion instanceof Ubicacion) 
//		{
//			ubi=ubicacion as Ubicacion
//		} this.nombre.equals(ubi.nombre)
//		
//	}
}

@Accessors 
class UbicacionVirtual extends Ubicacion{
	List<Ubicacion> ubicaciones
}
