package rentautoDAOS

import ar.edu.unq.epers.model.Auto
import org.hibernate.Session
import org.eclipse.xtext.xbase.lib.Functions.Function1
import java.util.List
import java.util.Date
import org.hibernate.Query

class AutoDAO {
	var Session session
	new()
	{
		
		session=SessionManager.getSession 
	}
	
	def guardar(Auto auto) {
		
		session.saveOrUpdate(auto)
		
		}
		
	def List<Auto> obtenerAutosQueNoEstanReservados(Date fecha)
	{
		val Query q = session.createQuery("from Auto as auto where not exists 
					(select autos from Reserva r left join r.auto as autos where
					  r.inicio < :fechaIndicada and r.fin >:fechaIndicada)
					  ")
		q.setParameter("fechaIndicada",fecha)
		q.list	
	}
	
	
	def obtenerAutosQueNuncaFueronReservados() {
	
		val Query q = session.createQuery("from Auto as auto where auto.reservas.size = 0")
		q.list
	}
	
	
	
}