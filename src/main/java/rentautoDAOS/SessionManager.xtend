package rentautoDAOS

import org.hibernate.SessionFactory
import org.hibernate.Transaction
import org.hibernate.cfg.Configuration
import org.hibernate.classic.Session
import org.eclipse.xtext.xbase.lib.Functions.Function1


class SessionManager {
	private static SessionFactory sessionFactory
	private static ThreadLocal<Session> tlSession = new ThreadLocal<Session>();
	
	def synchronized static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			var Configuration cfg = new Configuration();
			cfg.configure();

			sessionFactory = cfg.buildSessionFactory();
		}

		return sessionFactory;
	}
	
	def public static <T> T runInSession(Function1<  String,  T > cmd){
		var SessionFactory sessionFactory = SessionManager.getSessionFactory();
		var Transaction transaction = null;
		var T result = null;
		var Session session = null;
		
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();

			tlSession.set(session);
			
			result = cmd.apply("hola");

			session.flush();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			if (session != null)
				session.close();
			tlSession.set(null);
		}
		
		return result;
	}
	
	def static getSession() {
		return tlSession.get()
	}
	
	
}