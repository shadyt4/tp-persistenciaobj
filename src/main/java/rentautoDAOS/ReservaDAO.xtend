package rentautoDAOS


import ar.edu.unq.epers.model.Reserva
import java.util.Date
import ar.edu.unq.epers.model.Categoria
import ar.edu.unq.epers.model.Auto
import org.hibernate.Query
import java.util.List
import ar.edu.unq.epers.model.Ubicacion
import org.hibernate.Hibernate
import ar.edu.unq.epers.model.Empresa
import org.hibernate.Session

class ReservaDAO {
	
	var String queryString 
	var Query q  
	var Session session
	
	new()
	{
		//queryString="select autos from Reserva  r left join r.auto as autos where "
		//queryParaAutos= SessionManager.getSession().createQuery(queryString)
		session=SessionManager.getSession 
	}
	
	def List<Reserva>dameLasReservasConElDia(Date unDia, Date otroDia,String fechaInicio,String fechaFin)
	{
		
		//Create the query, with any arbitrary restriction or projection of data that
//you want to retrieve.
		var String queryString= "from Reserva r where r."+fechaInicio+ " = :fecha and r."+fechaFin+"=:otraFecha"	
		var Query query=SessionManager.getSession().createQuery(queryString)
										.setDate("fecha",unDia)
										.setDate("otraFecha",otroDia)
										
		query.list
	//lista la query
	}
	def List<Reserva>dameLasReservasConLaUbicacion(Ubicacion unaUbicacion,Ubicacion otra_Ubicacion, String ubicacionDeOrigen, String ubicacionDeDestino)
	{
		
		//Create the query, with any arbitrary restriction or projection of data that
//you want to retrieve.
		var String queryString= "from Reserva r where r."+ubicacionDeOrigen+ " = :ubicacion and r."+ubicacionDeDestino+"=:otraUbicacion"	
		var Query query=SessionManager.getSession().createQuery(queryString)
									  .setParameter("ubicacion",unaUbicacion,Hibernate.entity(Ubicacion))
									  .setParameter("otraUbicacion",otra_Ubicacion,Hibernate.entity(Ubicacion))
									  
		query.list
	//lista la query
	}
	
	
	def get(Integer id)
	{
		return SessionManager.getSession().get(typeof(Reserva), id)
	}
	
	def save(Reserva reserva){
		SessionManager.getSession().saveOrUpdate(reserva)
		//this.obternerReservasDelAuto(reserva.auto) //dudas, dudas revisar , si no hago esto salta el lazyinitialization exception 
		reserva.reservar
		
	}
	
//	def obternerReservasDelAuto(Auto auto) {
//		var Query q = SessionManager.getSession.createQuery("from Reserva r where r.auto.idAuto = :idA")
//		q.setParameter("idA", auto.idAuto)
//		auto.reservas=q.list // con esto activo las reservas
//	}
//	
//	def obtenerReservaDeLaEmpresa(Empresa empresa)
//	{
//		var Query q = SessionManager.getSession.createQuery("from Reserva r where r.usuario.getNombre()= :nombre_empresa ")
//		q.setParameter("nombre_empresa",empresa.nombreEmpresa)
//		empresa.reservas=q.list
//	}
	
	def obtenerReservaPorId(Integer id) {
		
		q = session.createQuery("from Reserva r where r.idReserva= :idReserva ")
		q.setParameter("idReserva",id)
		q.list.get(0) as Reserva
	}
	
	
	
	def List<Auto> obtenerAutosPorUbicacionDestino(Ubicacion ubicacion) {
		q = session.createQuery("select autos from Reserva  r left join r.auto as autos where r.destino=:ubDes")
		q.setParameter("ubDes",ubicacion)
		q.list 
		
	}
	
	def List<Auto>obtenerAutosPorFechaFinDeReserva(Date endDate) {
		q = session.createQuery("select autos from Reserva  r left join r.auto as autos where r.fin=:fechaFin")
		q.setParameter("fechaFin",endDate)
		q.list
	}
	
	
//	
//	def setQueryString(String string) {
//		this.queryString+string
//	}
//	
//	def executeQuery()
//	{
//		this.queryParaAutos=SessionManager.getSession().createQuery(queryString)
//	}
	
	
	
	def List<Auto>obtenerAutosPorFechaComienzoDeReserva(Date beginDate) {
		q = session.createQuery("select autos from Reserva  r left join r.auto as autos where r.inicio=:fechaInicio")
		q.setParameter("fechaInicio",beginDate)
		q.list
	}
	
	def List<Auto> obtenerAutosQueEstanLibresLaFechaIndicada (Date fechaIndicada){
		
		q = session.createQuery("select autos from Reserva r left join r.auto as autos where r.fin < :fechaIndicada
			and r.inicio > :fechaIndicada or r.inicio > :fechaIndicada and r.fin > :fechaIndicada" )
			q.setParameter("fechaIndicada",fechaIndicada)
			q.list
	}
	
//	def List<Auto> obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado (Date fechaIndicada, Ubicacion ubicacionIndicada){
//		q = session.createQuery("select autos from Reserva r left join r.auto as autos where r.fin <= :fechaIndicada
//			and r.inicio > :fechaIndicada or r.inicio > :fechaIndicada and r.fin > :fechaIndicada and  r.destino=:ubDes" )
//			q.setParameter("fechaIndicada",fechaIndicada)
//			q.setParameter("ubDes",ubicacionIndicada)
//			q.list
//	}
	
	def List<Auto> obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado (Date fechaIndicada, Ubicacion ubicacionIndicada){
		q = session.createQuery("select auto from Auto as auto left join auto.reservas as r  where 
								(auto.ubicacionInicial = :ubDes and r is null)
								or(r.destino = :ubDes and r.inicio < :fechaIndicada and r.fin <= :fechaIndicada)
								or(r.destino = :ubDes and r.inicio > :fechaIndicada and r.fin > :fechaIndicada) " )
			q.setParameter("fechaIndicada",fechaIndicada)
			q.setParameter("ubDes",ubicacionIndicada)
			q.list
	}
	
	def List<Auto> obtenerAutosPorDatosDeReserva (Ubicacion ori, Ubicacion dest, Date inicio, Date fin, Categoria cat)
	{
		q =session.createQuery("select autos from Reserva r left join r.auto as autos where r.fin = :fechaFin
			and r.inicio = :fechaInicio and  r.destino=:ubDes and r.origen =:ubOr and autos.categoria = :cat")
			q.setParameter("fechaFin",fin)
			q.setParameter("fechaInicio",inicio)
			q.setParameter("ubDes",dest)
			q.setParameter("ubOr",ori)
			q.setParameter("cat",cat)
			q.list
	}
	
	def List <Auto> obtenerAutosPorCategoria(Categoria categoria)
	{
		this.q =session.createQuery("select autos from Reserva r left join r.auto as autos where autos.categoria = :categoria")
		this.q.setParameter("categoria",categoria)		
		this.q.list
		
	}
	
}
