package filtro


import java.util.List

abstract class  Filtro {
	
	def abstract boolean getCondicion()
	
	def abstract <T> List<T> getCondition()
	
}