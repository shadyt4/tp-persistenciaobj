package filtro

import filtro.FiltroCompuesto
import java.util.Set
import java.util.HashSet
import java.util.List
import java.util.ArrayList

class FiltroOr extends FiltroCompuesto {
	
	new(Filtro filtro, Filtro filtro2) {
		super(filtro, filtro2)
	}
	
	override getCondicion() {
		return (consulta1.getCondicion() || consulta2.getCondicion())
		
	}
	
	override <T> List<T> getCondition(){
		
		var List<T> list = new ArrayList<T>();

        for (T t : consulta1.getCondition()) {
            if(!consulta2.getCondition().contains(t)) {
                list.add(t);
            }
        }

        return list
		
	}
	

	
}