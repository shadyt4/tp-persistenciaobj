package filtro

import filtro.Filtro

abstract class FiltroCompuesto extends Filtro {
	
	var package Filtro consulta1
	var package Filtro consulta2
	
	new(Filtro filtro, Filtro filtro2) {
		consulta1 = filtro
		consulta2 = filtro2 
	}
	
}