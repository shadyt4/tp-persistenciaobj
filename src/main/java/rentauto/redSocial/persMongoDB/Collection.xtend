package rentauto.redSocial.persMongoDB

import java.util.List
import net.vz.mongodb.jackson.JacksonDBCollection
import net.vz.mongodb.jackson.MapReduce

class Collection<T> {
	private JacksonDBCollection<T, String> mongoCollection;
	
	new(JacksonDBCollection<T, String> collection){
		this.mongoCollection = collection
	}
	
	def insert(T object){		
		return mongoCollection.insert(object);
    }

	def getMongoCollection(){
		return mongoCollection;
	}
}
