package rentauto.redSocial.model

import rentauto.redSocial.persMongoDB.SistemDB
import net.vz.mongodb.jackson.DBQuery
import com.mongodb.BasicDBObject
import java.util.ArrayList
import java.util.List

class ScoreManager {
	var homeRSScore = SistemDB.instance().collection(RedSocialScore)
	
	new(){
		
	}
	
	def saveScore(RedSocialScore rsScore) {
		homeRSScore.insert(rsScore)
	}
	
	def getScoreForUser(RedSocialUser user){

		homeRSScore.getMongoCollection().find(DBQuery.is("userId", user.id))
	}
	
	// PUNTAJES PUBLICOS
	def getPublicScoreForUser(RedSocialUser user) {
		var obj = createBasicDBObject(user, Visibilidad.PUBLICO) // mi objeto para compara por publico
		(homeRSScore.getMongoCollection().find(obj) as Iterable<RedSocialScore>).toList()		
	}
		
	// PUNTAJE PRIVADOS
	def getPrivateScoreForUser(RedSocialUser user) {
		var obj = createBasicDBObject(user, Visibilidad.PRIVADO)// mi objeto para compara por privado
		(homeRSScore.getMongoCollection().find(obj) as Iterable<RedSocialScore>).toList()	
	}
	// PUNTAJE AMIGOS
	def getFriendScoreForUser(RedSocialUser user){
		var obj = createBasicDBObject(user, Visibilidad.SOLO_AMIGOS)// mi objeto para compara por amistadad
		(homeRSScore.getMongoCollection().find(obj) as Iterable<RedSocialScore>).toList()
	}
	
	def createBasicDBObject(RedSocialUser user, Visibilidad visibilidad) {
		var obj = new BasicDBObject()
		obj.put("userId", user.id)
		obj.put("visibility", new BasicDBObject("$eq", visibilidad))
		obj
	}
	
	
	
	def getScoresOfFriends(ArrayList<RedSocialUser> users) {
		var List<RedSocialScore> retList = new ArrayList()
		for(u:users){
			retList.addAll(getFriendScoreForUser(u))
			retList.addAll(getPublicScoreForUser(u))
			}
		retList
	}
	
		def cleanDB() {
		homeRSScore.mongoCollection.drop
	}
	
}