package rentauto.redSocial.model;

public enum Visibilidad {
    PUBLICO, PRIVADO, SOLO_AMIGOS;

    public boolean noSoyPrivado() {
            return this != PRIVADO;
    }

    public boolean noSoyPrivadoNiAmigo() {
            return this == PUBLICO;
    }
}
