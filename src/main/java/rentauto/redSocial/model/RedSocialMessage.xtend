package rentauto.redSocial.model

import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.EqualsHashCode

@Accessors
@EqualsHashCode
class RedSocialMessage {
	
	String body
	
	new(String cuerpo){
		body = cuerpo
	}	
	
}