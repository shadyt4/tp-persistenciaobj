package rentauto.redSocial.model

import ar.edu.unq.epers.model.Auto
import net.vz.mongodb.jackson.ObjectId
import org.codehaus.jackson.annotate.JsonProperty
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.EqualsHashCode

@Accessors
@EqualsHashCode
class RedSocialScore {
	
	Integer userId
	String autoId
	Score score
	String coment
	Visibilidad visibility
	@ObjectId
	@JsonProperty("_id")
	String id

	new() {
	}
		
	new(RedSocialUser u, Auto a, Score s, String c,Visibilidad v){
		userId = u.id
		autoId = a.patente
		score = s
		coment = c
		visibility = v
	}
	
}

