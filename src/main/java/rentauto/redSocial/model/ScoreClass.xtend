package rentauto.redSocial.model

import net.vz.mongodb.jackson.ObjectId
import org.codehaus.jackson.annotate.JsonProperty

class ScoreClass {
	String s
	@ObjectId
	@JsonProperty("_id")
	String id
	
	new(String string) {
		s = string
	}
	
	new() {
	}
	
}

class Excelente extends ScoreClass{
	
	new(){
		super("excelente")
	}
}

class Bueno extends ScoreClass{

	new(){
		super("bueno")
	}
	
}

class Regular extends ScoreClass{

	new(){
		super("regular")
	}
}

class Malo extends ScoreClass{

	new(){
		super("malo")
	}
}
