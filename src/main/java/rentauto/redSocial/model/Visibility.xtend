package rentauto.redSocial.model

import net.vz.mongodb.jackson.ObjectId
import org.codehaus.jackson.annotate.JsonProperty

class Visibility {
	String s
	@ObjectId
	@JsonProperty("_id")
	String id
	
	new() {
	}
	
	new(String string) {
		s = string
	}
	
}


class PublicVisibility extends Visibility{
	new(){
		super("Publico")
	}
}

class PrivateVisibility extends Visibility{
	new(){
		super("privado")
	}
}

class FriendsVisibility extends Visibility{
	new(){
		super("solamente amigos")
	}
}