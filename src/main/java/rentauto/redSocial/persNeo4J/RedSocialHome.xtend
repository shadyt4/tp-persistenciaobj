package rentauto.redSocial.persNeo4J

import org.neo4j.graphdb.GraphDatabaseService
import org.neo4j.graphdb.DynamicLabel
import rentauto.redSocial.model.RedSocialUser
import rentauto.redSocial.model.RedSocialMessage
import org.neo4j.graphdb.Node
import org.neo4j.graphdb.RelationshipType
import org.neo4j.graphdb.Direction
import java.util.ArrayList
import java.util.List
import org.neo4j.graphdb.traversal.Traverser
import org.neo4j.graphdb.traversal.TraversalDescription
import org.neo4j.graphdb.traversal.Evaluators

class RedSocialHome {

	GraphDatabaseService graphDS

	new(GraphDatabaseService service) {
		graphDS = service
	}

	def personLabel() {
		DynamicLabel.label("user")
	}

	def messageLabel() {
		DynamicLabel.label("message")
	}

	def getNodo(RedSocialUser user) {
		graphDS.findNodes(personLabel, "id", user.id).head
	}

	def crearNodo(RedSocialUser user) {
		val node = graphDS.createNode(personLabel)
		node.setProperty("nombre", user.nombre)
		node.setProperty("id", user.id)
	}

	def crearNodoMensaje(RedSocialMessage msg) {
		val msgNode = graphDS.createNode(messageLabel)
		msgNode.setProperty("body", msg.body)
	}

	def getNodoMensaje(RedSocialMessage msg) {
		graphDS.findNodes(messageLabel, "body", msg.body).head
	}

	def amigo(RedSocialUser user, RedSocialUser user2) {
		val userNode = getNodo(user)
		val user2Node = getNodo(user2)

		userNode.createRelationshipTo(user2Node, TipoDeRelaciones.AMIGO_DE)
	}

	def enviarMensaje(RedSocialUser user1, RedSocialMessage msg, RedSocialUser user2) {
		var userNode = getNodo(user1)
		var user2Node = getNodo(user2)

		val msgNode = graphDS.createNode(messageLabel) => [setProperty("body", msg.body)]

		userNode.createRelationshipTo(msgNode, TipoDeRelaciones.ENVIADO)
		user2Node.createRelationshipTo(msgNode, TipoDeRelaciones.RECIBIDO)
	}

	def getAmistades(RedSocialUser user) {
		nodosRelacionados(getNodo(user), TipoDeRelaciones.AMIGO_DE, Direction.BOTH)
	}

	protected def nodosRelacionados(Node nodo, RelationshipType tipo, Direction direccion) {
		nodo.getRelationships(tipo, direccion).map[it.getOtherNode(nodo)]
	}

	def eliminarNodo(RedSocialUser user) {
		val nodo = getNodo(user)
		nodo.relationships.forEach[delete]
		nodo.delete
	}

	def mensajesEnviados(RedSocialUser user) {
		trasformarNodeAMensajes(nodosRelacionados(getNodo(user), TipoDeRelaciones.ENVIADO, Direction.OUTGOING))
	}

	def trasformarNodeAMensajes(Iterable<Node> l) {
		var listRes = new ArrayList()
		for (node : l) {
			listRes.add(crearMensajeDelNodo(node))
		}

		listRes
	}

	def RedSocialMessage crearMensajeDelNodo(Node node) {
		return new RedSocialMessage(node.getProperty("body") as String)
	}

	def mensajesRecibidos(RedSocialUser user) {
		trasformarNodeAMensajes(nodosRelacionados(getNodo(user), TipoDeRelaciones.RECIBIDO, Direction.OUTGOING))
	}

	def getAmigosDeLosAmigosDe(RedSocialUser user) {
		graphDS.execute("MATCH (user { id: " + user.id + " })-[:AMIGO_DE*0..3]-(friend_of_friend)
						 WHERE NOT (user)-[:AMIGO_DE]-(friend_of_friend)
						 RETURN friend_of_friend.name")
	}

	def Traverser amigosYAmigosDeLosAmigosDe(RedSocialUser user){
		var nu = getNodo(user)
	    var TraversalDescription td = graphDS.traversalDescription()
	            .breadthFirst()
	            .relationships( TipoDeRelaciones.AMIGO_DE, Direction.OUTGOING )
	            .evaluator( Evaluators.excludeStartPosition() );
	   td.traverse(nu);
}
	
	
}