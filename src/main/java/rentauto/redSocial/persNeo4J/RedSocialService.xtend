package rentauto.redSocial.persNeo4J

import org.neo4j.graphdb.GraphDatabaseService
import rentauto.redSocial.model.RedSocialUser
import rentauto.redSocial.persNeo4J.Neo4JService
import rentauto.redSocial.model.RedSocialMessage
import org.neo4j.graphdb.Node

class RedSocialService {
	
	def home(GraphDatabaseService graph) {
		new RedSocialHome(graph)
	}
	
	def getNodo(RedSocialUser user) {
		Neo4JService.run[home(it).getNodo(user)]
	}
	
	
	def crearNodo(RedSocialUser user) {
		Neo4JService.run[home(it).crearNodo(user); null]
	}
	
	def crearNodoMensaje(RedSocialMessage msg){
		Neo4JService.run[home(it).crearNodoMensaje(msg); null]
	}
	
	def generarAmistad(RedSocialUser user1, RedSocialUser user2) {
		Neo4JService.run[home(it).amigo(user1, user2)]
	}
	
	def getAmistades(RedSocialUser user) {
		Neo4JService.run[home(it).getAmistades(user).toList]
	}
	
	def eliminarNodo(RedSocialUser user) {
		Neo4JService.run[home(it).eliminarNodo(user); null]
	}
	
	def guardarMensaje(RedSocialUser userEmisor,RedSocialMessage msg, RedSocialUser userReceptor){
		Neo4JService.run[home(it).enviarMensaje(userEmisor, msg, userReceptor)]
	}
	
	def mensajesEnviadosPor(RedSocialUser user) {
		Neo4JService.run[home(it).mensajesEnviados(user)]
	}
	
	def mensajesRecibidosPor(RedSocialUser user) {
		Neo4JService.run[home(it).mensajesRecibidos(user)]
	}
	
	def amigosDeLosAmigosDe(RedSocialUser user) {
		Neo4JService.run[home(it).getAmigosDeLosAmigosDe(user).toList]
	}
	
	def amigosyamigosDeLosAmigosDe(RedSocialUser user) {
		Neo4JService.run[home(it).amigosYAmigosDeLosAmigosDe(user).toList]
	}
	
}