package rentauto.redSocial.persNeo4J;

import org.neo4j.graphdb.RelationshipType;

public enum TipoDeRelaciones implements RelationshipType {
	AMIGO_DE, ENVIADO, RECIBIDO
}