package rentauto

interface EnviadorDeMails {
	
	def void enviarMail(Mail mail) throws EnviarMailException
	
}