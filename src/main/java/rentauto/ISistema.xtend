package rentauto

interface ISistema {
	def void registrarUsuario(Usuario usuarioNuevo) throws UsuarioYaExisteException
	def void validarCuenta(String codigoDeValidacion)throws ValidacionException
	def Usuario ingresarUsuario(String userName, String password) throws UsuarioNoExiste
	def void cambiarPassword(String userName, String password, String nuevaPassword)
}

