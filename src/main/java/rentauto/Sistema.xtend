package rentauto

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.joda.time.DateTime
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException
import org.eclipse.xtext.xbase.lib.Functions.Function1
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class Sistema implements ISistema {


 var EnviadorDeMails em 
 
 	new()
 	{em= new EnviadorDeMailsImp()}
 
 

	override registrarUsuario(Usuario usuarioNuevo) {

		/*Como usuario quiero poder registrarme cargando mis datos y que quede registrado en el 
sistema. Cuando el usuario se registra debe enviar un mail al usuario para validar su 
cuenta. Para eso debe generarse un código de validación que se envía por mail. 
		 */
		if(existeUsuario(usuarioNuevo) )throw new UsuarioYaExisteException
		ejecutar([conn|
			var PreparedStatement ps = conn.prepareStatement(
			"INSERT INTO Usuarios (NOMBRE,APELLIDO,NOMBREDEUSUARIO,EMAIL,PASSWORD,FECHADENACIMIENTO,CODIGODEVALIDACION) VALUES (?,?,?,?,?,?,?)");
			ps.setString(1, usuarioNuevo.nombre);
			ps.setString(2, usuarioNuevo.apellido)
			ps.setString(3, usuarioNuevo.nombreDeUsuario);
			ps.setString(4, usuarioNuevo.EMail);
			ps.setString(5, usuarioNuevo.password);
			ps.setString(6, usuarioNuevo.fechaDeNacimiento.toString);
			ps.setString(7, usuarioNuevo.nombreDeUsuario + usuarioNuevo.fechaDeNacimiento.toString)

			ps.execute()
			
			val mail = new Mail(usuarioNuevo.nombreDeUsuario + usuarioNuevo.fechaDeNacimiento.toString,
			"codigo de validacion", usuarioNuevo.EMail, "sistemaMail")
			em.enviarMail(mail)
			null	
			//
			
		])
		
		
	}

	override validarCuenta(String codigoDeValidacion) {
		ejecutar([conn| 
			var String codigo
			var PreparedStatement ps = conn.prepareStatement("SELECT CODIGODEVALIDACION FROM Usuarios WHERE CODIGODEVALIDACION =?");
			ps.setString(1, codigoDeValidacion);
			var ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				codigo = rs.getString("CODIGODEVALIDACION")
			}
			if(codigo != codigoDeValidacion) throw new ValidacionException			
			
		])
	}


	def existeUsuario(Usuario usuario)
	{
		ejecutar([conn|
			var String userName
			var PreparedStatement ps = conn.prepareStatement("SELECT NOMBREDEUSUARIO FROM Usuarios WHERE NOMBREDEUSUARIO = ?")
			ps.setString(1,usuario.nombreDeUsuario)
			var ResultSet rs = ps.executeQuery();
			if(rs.next())
			{
				userName=rs.getString("NOMBREDEUSUARIO")
				
			}
			
			userName!=null
		])
	}
	def <T> T ejecutar(Function1<Connection, T> bloque ) {
		var Connection conn = null;
		var PreparedStatement ps = null;
		try {
			conn = this.getConnection();
			return bloque.apply(conn)
			
		} finally {
			finallyBlock(conn, ps)
		}

	}

	override ingresarUsuario(String userName, String password) {
		
		ejecutar([conn | 
			var Usuario us	
			var PreparedStatement ps = conn.prepareStatement(
				"SELECT NOMBRE,APELLIDO,NOMBREDEUSUARIO,EMAIL,PASSWORD,FECHADENACIMIENTO FROM Usuarios WHERE NOMBREDEUSUARIO = ? AND PASSWORD =?");
			ps.setString(1, userName);
			ps.setString(2, password);

			var ResultSet rs = ps.executeQuery();

			var String nombre
			var String apellido
			var String nombreDeUsuario
			var String mail
			var String pass
			var DateTime fecha
			if (rs.next()) {
				us = new Usuario(rs.getString("NOMBRE"), rs.getString("APELLIDO"), rs.getString("NOMBREDEUSUARIO"),
					rs.getString("EMAIL"), rs.getString("PASSWORD"), new DateTime(rs.getString("FECHADENACIMIENTO")))
			}
		
		if (us == null) {
			throw new UsuarioNoExiste
		} else {

			return us
		}
		
		])
		}

	override cambiarPassword(String userName, String password, String nuevaPassword) {

		/*UPDATE table_name
 SET column1=value1,column2=value2,...
 WHERE some_column=some_value; */
		if(password == nuevaPassword) throw new NuevaPasswordInvalida
		ejecutar([conn | 
		var PreparedStatement ps = conn.prepareStatement("UPDATE Usuarios SET PASSWORD = ? WHERE NOMBREDEUSUARIO = ? AND PASSWORD = ?");
		ps.setString(1, nuevaPassword)
		ps.setString(2, userName)
		ps.setString(3, password)
		ps.executeUpdate

		])
		 
	}

	def finallyBlock(Connection conn, PreparedStatement ps) {

		if (ps != null)
			ps.close();
		if (conn != null)
			conn.close();

	}

	def Connection getConnection() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		return DriverManager.getConnection("jdbc:mysql://localhost/Rentauto?user=root&password=root");
	}
	
	def void setEnviadorDeMails(EnviadorDeMails envmails)
 	{
 		em=envmails
 	}

}
