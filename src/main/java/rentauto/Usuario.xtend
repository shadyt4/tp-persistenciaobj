package rentauto

import org.joda.time.DateTime
import org.eclipse.xtend.lib.annotations.Accessors
import ar.edu.unq.epers.model.IUsuario
import ar.edu.unq.epers.model.Reserva
import java.util.List

@Accessors
class Usuario implements IUsuario{
	 String nombre
	 String apellido
	 String nombreDeUsuario
	 String eMail
	 String password
	 DateTime fechaDeNacimiento
	 List<Reserva> reservas
	 Integer idUsuario
	 
	new(String name, String lastName, String userName, String mail, String pass, DateTime birthDay)
	{
		nombre=name
		apellido=lastName
		nombreDeUsuario=userName
		eMail=mail
		password=pass
		fechaDeNacimiento=birthDay
	}
	
	override agregarReserva(Reserva unaReserva) {
		reservas.add(unaReserva)
	}
	
	override getReservas() {
		return reservas
	}
	
	override getIdUsuario() {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
}


