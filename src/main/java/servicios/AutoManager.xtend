package servicios

import ar.edu.unq.epers.model.Auto
import rentautoDAOS.SessionManager
import rentautoDAOS.AutoDAO
import java.util.List
import org.eclipse.xtext.xbase.lib.Functions.Function1
import java.util.Date
import rentautoDAOS.ReservaDAO
import ar.edu.unq.epers.model.Ubicacion

class AutoManager {
	
	public (Auto)=>Auto guardar =[Auto auto|
		new AutoDAO().guardar(auto)
		auto]

	public (Date)=>List<Auto> autosQueNoTienenReservas=[Date d|
		var autosR=new ReservaDAO().obtenerAutosQueEstanLibresLaFechaIndicada(d)
		autosR.addAll(autosQueNoTienenNingunaReservas.apply)
		autosR
	]	
	
	public (Date, Ubicacion)=>List<Auto>obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado=[Date diaIndicado,Ubicacion indicada|
		var autosR=new ReservaDAO().obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado(diaIndicado,indicada)
		autosR
	]
	
	public ()=>List<Auto >autosQueNoTienenNingunaReservas =[new AutoDAO().obtenerAutosQueNuncaFueronReservados()]
	
	public (Integer)=>List<Auto> obtenerAutoPorId=[Integer id|new AutoDAO().obtener_auto_por_id(id)]
	
	def  guardarAuto(Auto auto)
	{
		SessionManager.runInSession([this.guardar.apply(auto)])
	}
	
	def  obtenerAutos(Function1<String,List<Auto>> f)
	{
		SessionManager.runInSession(f)
					
	}
	
}