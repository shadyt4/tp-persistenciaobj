package servicios

import ar.edu.unq.epers.model.Auto
import java.util.List
import ar.edu.unq.epers.model.Ubicacion
import java.util.Date
import rentautoDAOS.SessionManager
import ar.edu.unq.epers.model.IUsuario
import org.hibernate.Hibernate
import ar.edu.unq.epers.model.Reserva
import org.eclipse.xtext.xbase.lib.Functions.Function1
import rentautoDAOS.ReservaDAO
import ar.edu.unq.epers.model.Categoria

class ReservaManager {
	
	public (Reserva)=>Reserva hacerReserva=[Reserva r|
		 new ReservaDAO().save(r)
		r]
	public (Ubicacion )=>List<Auto> obtenerAutosPorUbicacion=[Ubicacion u|
		new ReservaDAO().obtenerAutosPorUbicacionDestino(u)
	]
	
	public (Date )=>List<Auto> obtenerAutosPorFechaFinDeReserva=[Date d|
		new ReservaDAO().obtenerAutosPorFechaFinDeReserva(d)
	]
	
	public (Date)=>List<Auto> obtenerAutosPorFechaCominezoDeReserva=[Date d|
		new ReservaDAO().obtenerAutosPorFechaComienzoDeReserva(d)
	]
	
	public (Date)=>List<Auto> obtenerAutosQueEstanLibresLaFechaIndicada = [Date d|
		new ReservaDAO().obtenerAutosQueEstanLibresLaFechaIndicada(d)
	]
	
	public (Date, Ubicacion) => List<Auto> obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado = [Date d, Ubicacion u |
		new ReservaDAO().obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado(d,u)
	]
	
	public (Ubicacion,Ubicacion,Date,Date,Categoria) => List<Auto> obtenerAutosPorDatosDeReserva = [Ubicacion ori, Ubicacion dest, Date inicio, Date fin, Categoria categoria|
		new ReservaDAO().obtenerAutosPorDatosDeReserva(ori,dest,inicio,fin,categoria)
		]
		
	public (Categoria ) => List<Auto> obtenerAutosPorCategoria = [Categoria cat| 
		new ReservaDAO().obtenerAutosPorCategoria(cat)
	]	
	def obtenerAutos(Function1<String,List<Auto>> f)
	{
		SessionManager.runInSession(f)
		
	}
	
	def guardarReserva(Reserva reserva)
	{
		SessionManager.runInSession([this.hacerReserva.apply(reserva)])
	}
	
	
	
	
	

	
}