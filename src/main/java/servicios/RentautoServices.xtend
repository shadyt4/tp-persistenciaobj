package servicios

import ar.edu.unq.epers.model.Reserva
import cacheHome.CacheRentautoDAO
import cacheHome.InsertarAutos
import cacheServices.Servicios
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.client.HBaseAdmin

class RentautoServices {
	
	var AutoManager auto_manager = new AutoManager()
	var ReservaManager reserva_manager
	new () {
		auto_manager = new AutoManager()
		reserva_manager=new ReservaManager()
	} 
	
	def reservar (Reserva reserva)
	{
		reserva_manager.guardarReserva(reserva)
		
			CacheRentautoDAO.actualizar_tabla("rentauto")
		}
	
	
}