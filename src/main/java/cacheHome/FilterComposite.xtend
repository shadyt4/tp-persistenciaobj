package cacheHome

import org.apache.hadoop.hbase.filter.FilterList
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class FilterComposite {
	
//	var FilterList filterList
//	new()
//	{ filterList = new FilterList();
//	}
	def static componer_filters( SingleColumnValueFilter filter1, SingleColumnValueFilter filter2)
	{	var FilterList filterList  = new FilterList();
		filterList.addFilter(filter1)
		filterList.addFilter(filter2)
		return filterList		
	}
	
	def static componer_filters(SingleColumnValueFilter filter1, SingleColumnValueFilter filter2,SingleColumnValueFilter filter3)
	{var FilterList filterList  = new FilterList();

		filterList.addFilter(filter1)
		filterList.addFilter(filter2)
		filterList.addFilter(filter3)
		return filterList		
	}
	
	
}