package cacheHome

import java.util.List
import ar.edu.unq.epers.model.Auto
import java.util.Collection
import com.google.gson.Gson
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.client.Put
import java.util.ArrayList
import java.util.Date
import ar.edu.unq.epers.model.Ubicacion
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.client.HTable

class InsertarAutos {
	def static List<Auto> insertar(List<Auto> autos, Date dateDisponible, Ubicacion ubicacionDisponible,
		Configuration conf, String nombreDeLaTabla, String columnFamilyName, String fecha, String ubicacion, String auto) {

		var HTable table = new HTable(conf, nombreDeLaTabla);
		var Collection<Auto> autosParaInsertar = copiarListaDeAutos(autos)

		//creo un Gson para hacer un toJson a la lista
		var Gson transformador = new Gson()

		//guardo el json en un string
		var String glist = transformador.toJson(autosParaInsertar)

		var dateDisponibleJson = transformador.toJson(dateDisponible)
		var ubicacionDisponibleJSTR = transformador.toJson(ubicacionDisponible)

		// con esto se agrega una row key
		var Put put1 = new Put(Bytes.toBytes(ubicacionDisponible.nombre + dateDisponibleJson));

		//asi se guarda en la tabla
		put1.addColumn(Bytes.toBytes(columnFamilyName), Bytes.toBytes(fecha), Bytes.toBytes(dateDisponibleJson));
		put1.addColumn(Bytes.toBytes(columnFamilyName), Bytes.toBytes(ubicacion), Bytes.toBytes(ubicacionDisponibleJSTR));

		put1.addColumn(Bytes.toBytes(columnFamilyName), Bytes.toBytes(auto), Bytes.toBytes(glist));

		//guardo otra row
		//guardo en la column family autos, en la la columna fecha, la fecha
		table.put(put1);
		
		table.flushCommits();

		table.close();
		autosParaInsertar.toList

	}

	def static List<Auto> copiarListaDeAutos(List<Auto> autos) {
		var List<Auto> autosCopy = new ArrayList<Auto>()
		for (auto : autos) {
			autosCopy.add(
				new Auto => [idAuto = auto.idAuto marca = auto.marca modelo = auto.modelo anio = auto.anio
					patente = auto.patente costoBase = auto.costoBase])
		}

		return autosCopy
	}
}
