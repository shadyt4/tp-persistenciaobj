package cacheHome

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.client.HBaseAdmin

class BorrarTabla {
	
		
		def void borrar(Configuration conf, String nombreDeLaTabla)
		{
			var admin = new HBaseAdmin(conf);
			admin.disableTable(nombreDeLaTabla)
			admin.deleteTable(nombreDeLaTabla)	
		}
}