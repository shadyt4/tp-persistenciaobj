package cacheHome

import ar.edu.unq.epers.model.Auto
import ar.edu.unq.epers.model.Ubicacion
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.ArrayList
import java.util.Collection
import java.util.Date
import java.util.List
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.HColumnDescriptor
import org.apache.hadoop.hbase.HTableDescriptor
import org.apache.hadoop.hbase.client.HBaseAdmin
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.client.Result
import org.apache.hadoop.hbase.client.ResultScanner
import org.apache.hadoop.hbase.client.Scan
import org.apache.hadoop.hbase.filter.CompareFilter
import org.apache.hadoop.hbase.filter.Filter
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter
import org.apache.hadoop.hbase.filter.SubstringComparator
import org.apache.hadoop.hbase.util.Bytes
import servicios.AutoManager

class CacheRentautoDAO {

	def static List<Auto> obtenerAutosPorFechaYUbicacion(Date fecha, Ubicacion ubicacion, String nombreDeLaTabla,
		Configuration conf) {

		var HTable table = new HTable(conf, nombreDeLaTabla) // seria un table.open()

		//actualizarTabla(fecha,ubicacion,nombreDeLaTabla,conf)
		if (tamanho_de_la_tabla(conf, nombreDeLaTabla) == 0) {
			actualizarTabla(fecha, ubicacion, nombreDeLaTabla, conf)
		}
		var SingleColumnValueFilter filterFecha = filtrar_por_fecha(fecha)
		var SingleColumnValueFilter filterUbicacion = filtrar_por_ubicacion(ubicacion)
		FilterComposite.componer_filters(filterFecha, filterUbicacion)

		devolver_autos(table, scannear(FilterComposite.componer_filters(filterFecha, filterUbicacion)))
	}

	/**la idea de actualizar es 
	 * pedirle a la tabla de hibernate los autos disponibles por fecha y lugar,
	 * deberia hacer un actualizar fila y después actualizar tabla
	 * si la fila no existe la crea
	 */
	def static void actualizarTabla(Date fecha, Ubicacion ubicacion, String nombreDeLaTabla, Configuration conf) {
		var List<Auto> autos = new ArrayList<Auto>()
		var InsertarAutos ia = new InsertarAutos()
		val AutoManager autoManager = new AutoManager()
		autos = autoManager.obtenerAutos(
			[String a|autoManager.obtenerAutosQueEstanLibresLaFechaIndicadaYElLugarIndicado.apply(fecha, ubicacion)])
		InsertarAutos.insertar(autos, fecha, ubicacion, conf, nombreDeLaTabla, "autos", "fecha", "ubicacion", "auto")

	}

	/**
	 * cuando hay un cambio en la tabla de hibernate
	 * se borra la cache
	 * y se vuelve a crear vacia
	 */
	def static actualizar_tabla(String nombre_de_la_tabla) {

		var Configuration conf = HBaseConfiguration.create();
		var HBaseAdmin admin = new HBaseAdmin(conf);
		if (admin.tableExists("rentauto")) {
			var BorrarTabla br = new BorrarTabla()
			br.borrar(conf, nombre_de_la_tabla)

			
			conf.addResource("conf/hbase-site.xml");
			conf.set("hbase.master", "localhost:60000");

			//var admin = new HBaseAdmin(conf);
			var HTableDescriptor rentautoDesc = new HTableDescriptor("rentauto");
			var HColumnDescriptor autosHB = new HColumnDescriptor("autos");

			rentautoDesc.addFamily(autosHB);

			admin.createTable(new HTableDescriptor(rentautoDesc));
		}

	}

	/**
	 * Devuelve una lista de String como lista de autos
	 */
	def static List<Auto> deJsonAList(String listaJson) {
		var Gson gson = new Gson()
		var Type collectionType = new TypeToken<Collection<Auto>>() {
		}.getType
		gson.fromJson(listaJson, collectionType)

	}

	def static filtrar_por_fecha(Date fecha) {
		var gson = new Gson()
		var fechaJson = gson.toJson(fecha)
		var SingleColumnValueFilter filterFecha = new SingleColumnValueFilter(Bytes.toBytes("autos"),
			Bytes.toBytes("fecha"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(fechaJson));

		//filterFecha.setFilterIfMissing(true)
		return filterFecha
	}

	def static filtrar_por_ubicacion(Ubicacion ubicacion) {
		var gson = new Gson()
		var ubicacionJson = gson.toJson(ubicacion)
		var SingleColumnValueFilter filterUbicacion = new SingleColumnValueFilter(Bytes.toBytes("autos"),
			Bytes.toBytes("ubicacion"), CompareFilter.CompareOp.EQUAL, new SubstringComparator(ubicacionJson));

		//filterUbicacion.setFilterIfMissing(true)
		return filterUbicacion
	}

	def static scannear(Filter f) {
		var Scan scan = new Scan();
		scan.setFilter(f);

		//
		// Get Scanner Results
		return scan

	}

	def static devolver_autos(HTable table, Scan scan) {
		var ResultScanner scanner = table.getScanner(scan);
		var String listaDeAutosEnString
		for (Result res : scanner) {
			var byte[] resultValues = res.getValue(Bytes.toBytes("autos"), Bytes.toBytes("auto"))

			//esto debería ser una lista de autos en string
			listaDeAutosEnString = Bytes.toString(resultValues)

		}

		scanner.close();
		table.close();

		//return autosResult
		deJsonAList(listaDeAutosEnString)
	}

	def static tamanho_de_la_tabla(Configuration conf, String nombreDeLaTabla) {
		var HTable table = new HTable(conf, nombreDeLaTabla)
		var index = 0
		System.out.println("scanning full table:");
		var ResultScanner scanner = table.getScanner(new Scan());
		var Result rr = scanner.next()
		while (rr != null) {
			index = index + 1
			rr = scanner.next();
		}
		System.out.println(index)

		return index
	}

}
